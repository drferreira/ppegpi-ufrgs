libname EPI93 V9  "//home/diogorosasferr0/epi93/trabalho-final";

Ponderações sobre as variaveis
Desfecho: chd
Exposição: smoking
Confundimento: activity, systol, bmi, totchol, age

data exerc;
	set EPI93.exercicio_shhs_180321;
	
	label age = 'Idade (anos)';
	label totchol = 'Colesterol total (mmol/l)';
	label bmi = 'IMC (kg/m2)';
	label systol = 'PAS (mmHg)';
	label smoking = 'Tabagismo';
	label activity = 'Atividade fisico';
	label chd = 'DCV (1=Sim, 0=Nao)';
	label hypert_cat= 'Hipertensão categorica';
	label totchol_cat= 'Colesterol total categorica';
	label bmi_cat = 'IMC (kg/m2) categorica';
	
	
	/* Definição de variaveis categóricas */

	if not missing(systol) then hypert_cat = (systol >= 140);
	if not missing(totchol) then totchol_cat = (totchol >= 4.913);	
	if not missing(bmi)
		then if bmi < 25
			then bmi_cat = 1;
		else if bmi < 30
			then bmi_cat = 2;
		else bmi_cat = 3;
		

	ods noproctitle;	
	ods graphics / imagemap=on;
	proc means data=WORK.exerc chartype mean std min max median n vardef=df q1 q3;
		var age;
	run;
	
	proc means data=WORK.EXERC chartype mean std min max n vardef=df;
		var age ;
		class smoking;
	run;
	
	proc freq data=WORK.exerc;
		tables chd activity hypert_cat totchol_cat bmi_cat;
	run;
	
	ods noproctitle;
	proc freq data=WORK.exerc;
		tables  (chd activity hypert_cat totchol_cat bmi_cat)*(smoking) / nopercent nocol nocum;
	run;

	
	/* 	Construção de tabela 1 */
	ods graphics / reset width=6.4in height=4.8in imagemap;	
	proc sgplot data=WORK.EXERC;
		histogram age /;
		histogram totchol /;
		histogram systol /;
		density age;
		density totchol;
		density systol;
		yaxis grid;
	run;
	ods graphics / reset;
	ods noproctitle;
	ods graphics / imagemap=on;
	
	ods noproctitle;
	ods graphics / imagemap=on;
	
/* ================================================ */
/* Pressuposto de linearidade para idade
/* ================================================ */
proc rank data=work.exerc groups=4 ties=low out=quartis;
	ranks age_quartil;
	var age;
run;
data exerc;
	set quartis;
	age_quartil = age_quartil+1;
run;	
proc freq data=exerc;
	table age_quartil;
run;

/* Resultado log L 1135.927 */
proc logistic data=work.exerc descending;
	title 'Regressão logistica (Ordinal)';
	model chd = age_quartil / link=logit lackfit rl;
run;

/* Resultado log L 1130.696 */
proc logistic data=work.exerc descending;
	class age_quartil(ref="1") / param=ref;
	title 'Regressão logistica (Categorico)';
	model chd = age_quartil / link=logit lackfit rl;
run;
	
proc logistic data=work.exerc descending;
	

/* ================================================ */
/* MODELO sem Ajuste */
/* ================================================ */

	Modelo de regressão logistica utilizando log binominal
	ods noproctitle;
	ods graphics / imagemap=on;
	proc genmod data=WORK.exerc plots=(dfbetas h);
			class smoking(ref = "1")/ param=reference;
		model chd(event='1')=smoking / link=log dist=binomial;
		estimate "Ex-tabagista x Nunca Fumou" smoking 1 0 / exp;
		estimate "Tabagista x Nunca Fumou" smoking 0 1 / exp;
	run;
	



/* ================================================ */
/* MODELO ajustado pela idade */
/* ================================================ */

	Modelo de regressão logistica utilizando log binominal
	ods noproctitle;
	ods graphics / imagemap=on;
	proc genmod data=WORK.exerc plots=(dfbetas h);
		class smoking(ref = "1")/ param=reference;
		model chd(event='1')=smoking age / link=log dist=binomial;
		estimate "Ex-tabagista x Nunca Fumou" smoking 1 0 / exp;
		estimate "Tabagista x Nunca Fumou" smoking 0 1 / exp;
	run;

/* ================================================ */
/* MODELO ajustado pela idade, atividade, IMC, 
	colesterol e hipertensão*/
/* ================================================ */

	Modelo de regressão logistica utilizando log binominal
	ods noproctitle;
	ods graphics / imagemap=on;
	proc genmod data=WORK.exerc plots=(dfbetas h);
		class smoking(ref = "1") activity bmi_cat totchol_cat hypert_cat / param=reference;
		model chd(event='1')=smoking age activity bmi_cat totchol_cat hypert_cat  / link=log dist=binomial;
		estimate "Ex-tabagista x Nunca Fumou" smoking 1 0 / exp;
		estimate "Tabagista x Nunca Fumou" smoking 0 1 / exp;
	run;


